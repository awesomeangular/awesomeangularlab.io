import { Component, OnInit, Input } from '@angular/core';
import { Observable } from 'rxjs';
import { Month } from '../month.enum';

@Component({
  selector: 'aa-calender-month',
  templateUrl: './calender-month.component.html',
  styleUrls: ['./calender-month.component.sass']
})
export class CalenderMonthComponent implements OnInit {

  @Input() inputDate$: Observable<Date>;

  private selectedDate: Date;
  private selectedSecond: number;
  private selectedMonth: Month;
  private selectedYear: number;
  private daysInSelectedMonth: number;

  getDate(): void {
    this.inputDate$.subscribe((response: Date) => {
      this.selectedDate = response;
      this.selectedSecond = response.getUTCSeconds();
      this.selectedMonth = response.getUTCMonth() + 1;
      this.selectedYear = response.getUTCFullYear();
      this.daysInSelectedMonth = this.daysInMonth(this.selectedMonth, this.selectedYear);
    });
  }

  constructor() { }

  ngOnInit(): void {
    this.getDate();
  }

  // Month here is 1-indexed (January is 1, February is 2, etc). This is
  // because we're using 0 as the day so that it returns the last day
  // of the last month, so you have to add 1 to the month number 
  // so it returns the correct amount of days
  daysInMonth(month: number, year: number) {
    return new Date(year, month, 0).getDate();
  }

}
