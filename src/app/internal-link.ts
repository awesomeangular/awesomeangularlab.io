export class InternalLink {
    id: number;
    title: string;
    address: string;
    constructor(id: number, title: string, address: string) {
        this.id = id;
        this.title = title;
        this.address = address;
    }
}
