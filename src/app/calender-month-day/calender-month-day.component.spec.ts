import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CalenderMonthDayComponent } from './calender-month-day.component';

describe('CalenderMonthDayComponent', () => {
  let component: CalenderMonthDayComponent;
  let fixture: ComponentFixture<CalenderMonthDayComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CalenderMonthDayComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CalenderMonthDayComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
