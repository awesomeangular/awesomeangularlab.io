import { Component, OnInit } from '@angular/core';
import { InternalLink } from './internal-link';
import { Router } from '@angular/router';

@Component({
  selector: 'aa-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.sass']
})
export class AppComponent implements OnInit {
  title = 'AwesomeAngular';
  internalLinks: Array<InternalLink>;

  constructor(
    private router: Router
  ) { }

  ngOnInit(): void {
    const internalLink1 = new InternalLink(1, 'About 1', 'about');
    this.internalLinks = [];
    this.internalLinks.push(internalLink1,
      // internalLink2, internalLink3, internalLink4, internalLink5, internalLink6
    );
  }

  showInfo(internalLink: InternalLink) {
    console.log({ internalLink });
    this.router.navigateByUrl(`/${internalLink.address}`);
  }
}
