import { Component, OnInit, OnDestroy } from '@angular/core';
import { LoadingService } from '../loading.service';

@Component({
  selector: 'aa-page-not-found',
  templateUrl: './page-not-found.component.html',
  styleUrls: ['./page-not-found.component.sass']
})
export class PageNotFoundComponent implements OnInit, OnDestroy {

  constructor(
    private loadingService: LoadingService
  ) { }

  ngOnInit(): void {
    this.loadingService.turnLoadingOn();
  }

  ngOnDestroy(): void {
    this.loadingService.turnLoadingOff();
  }

}
