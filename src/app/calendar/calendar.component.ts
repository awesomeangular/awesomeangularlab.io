import { Component, OnInit } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';

@Component({
  selector: 'aa-calendar',
  templateUrl: './calendar.component.html',
  styleUrls: ['./calendar.component.sass']
})
export class CalendarComponent implements OnInit {

  // output behavior subject for children
  private inputDateBehaviorSubject: BehaviorSubject<Date>;
  public inputDate$: Observable<Date>;

  constructor() {
    this.inputDateBehaviorSubject = new BehaviorSubject(new Date());
    this.inputDate$ = this.inputDateBehaviorSubject as Observable<Date>;
  }

  ngOnInit(): void {
    setInterval(
      () => {
        this.inputDateBehaviorSubject.next(new Date());
      }, 1
     ) ;
  }

}
